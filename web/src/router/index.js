import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: () => import('@/views/dashboard.vue')
    },
    {
      path: '/users',
      name: 'users',
      component: () => import('@/views/users.vue')
    },
    {
      path: '/roles',
      name: 'roles',
      component: () => import('@/views/roles.vue')
    },
    {
      path: '/auths',
      name: 'auths',
      component: () => import('@/views/menus.vue')
    }
  ]
})

export default router
