const Koa = require('koa')
const app = new Koa()
const json = require('koa-json')
const onerror = require('koa-onerror')
const bodyparser = require('koa-bodyparser')
const logger = require('koa-logger')
const cors = require('koa2-cors')

require('./config/db.js')

const index = require('./routes/index')

// 404 Not Found
app.use(async (ctx, next) => {
    await next()
    if (ctx.status === 404) {
        ctx.throw(404)
    }
})

// error handler
onerror(app, {
    accepts() {
        return 'json';
    },
    json(err, ctx) {
        ctx.body = {
            code: ctx.status || 500,
            msg: err.message
        }
    }
})

app.use(cors())

// middlewares
app.use(bodyparser({
    enableTypes: ['json', 'form', 'text']
}))
app.use(json())
app.use(logger())
app.use(require('koa-static')(__dirname + '/public'))

// logger
app.use(async (ctx, next) => {
    const start = new Date()
    await next()
    const ms = new Date() - start
    console.log(`${ctx.method} ${ctx.url} - ${ms}ms`)
})

// routes
app.use(index.routes(), index.allowedMethods())

// error-handling
app.use((err, ctx) => {
    console.error('error: ', err.status, err.message)
})

module.exports = app
