const mongoose = require('mongoose')

const menuSchema = new mongoose.Schema({
  pid: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Menu'
  },
  path: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  title: String,
  icon: String
})

module.exports = mongoose.model('Menu', menuSchema)
