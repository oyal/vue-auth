const mongoose = require('mongoose')

const roleSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  menus: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Menu' }]
})

module.exports = mongoose.model('Role', roleSchema)
