const router = require('koa-router')()
const User = require('../models/User.js')
const Role = require('../models/Role.js')
const Menu = require('../models/Menu.js')

// ---------------------------- 用户管理 ---------------------------- //

router.get('/users/list', async ctx => {
    const {id} = ctx.query

    if (id) {
        const user = await User.findById(id).populate({
            path: 'roles',
            populate: {
                path: 'menus'
            }
        })
        ctx.body = {
            code: 200,
            msg: '查询成功',
            data: {
                user
            }
        }
    } else {
        const users = await User.find().populate('roles')
        ctx.body = {
            code: 200,
            msg: '查询成功',
            data: {
                users
            }
        }
    }

})

router.post('/users/addition', async ctx => {
    const {username, password} = ctx.request.body
    const isExist = await User.findOne({username})
    if (isExist) {
        ctx.status = 400
        return ctx.body = {
            code: 400,
            msg: '该用户已存在'
        }
    }
    const user = await User.create({username, password})
    ctx.body = {
        code: 200,
        msg: '创建成功',
        data: {
            user
        }
    }
})

router.put('/users/info', async ctx => {
    const {id} = ctx.query
    const {username, password} = ctx.request.body
    await User.findByIdAndUpdate(id, {username, password})
    ctx.body = {
        code: 200,
        msg: '修改成功',
        data: ''
    }
})

router.put('/users/roles', async ctx => {
    const {id} = ctx.query
    const {roles} = ctx.request.body
    const roleList = roles === '' ? [] : roles.split(',')
    await User.findByIdAndUpdate(id, {roles: roleList})
    ctx.body = {
        code: 200,
        msg: '修改成功',
        data: ''
    }
})

router.delete('/users/delete', async ctx => {
    const {id} = ctx.query
    await User.findByIdAndDelete(id)
    ctx.body = {
        code: 200,
        msg: '删除成功',
        data: ''
    }
})

// ---------------------------- 角色管理 ---------------------------- //

router.get('/roles/list', async ctx => {
    const roles = await Role.find().populate('menus')
    ctx.body = {
        code: 200,
        msg: '查询成功',
        data: {
            roles
        }
    }
})

router.post('/roles/addition', async ctx => {
    const {name} = ctx.request.body
    const isExist = await Role.findOne({name})
    if (isExist) {
        ctx.status = 400
        return ctx.body = {
            code: 400,
            msg: '该角色已存在'
        }
    }
    const role = await Role.create({name})
    ctx.body = {
        code: 200,
        msg: '创建成功',
        data: {
            role
        }
    }
})

router.put('/roles/info', async ctx => {
    const {id} = ctx.query
    const {name} = ctx.request.body
    await Role.findByIdAndUpdate(id, {name})
    ctx.body = {
        code: 200,
        msg: '修改成功',
        data: ''
    }
})

router.put('/roles/menus', async ctx => {
    const {id} = ctx.query
    const {menus} = ctx.request.body
    const menuList = menus === '' ? [] : menus.split(',')
    await Role.findByIdAndUpdate(id, {menus: menuList})
    ctx.body = {
        code: 200,
        msg: '修改成功',
        data: ''
    }
})

router.delete('/roles/delete', async ctx => {
    const {id} = ctx.query
    await Role.findByIdAndDelete(id)
    ctx.body = {
        code: 200,
        msg: '删除成功',
        data: ''
    }
})

// ---------------------------- 菜单管理 ---------------------------- //

router.get('/menus/list', async ctx => {
    const menus = await Menu.find()
    ctx.body = {
        code: 200,
        msg: '查询成功',
        data: {
            menus
        }
    }
})

router.get('/menus/one', async ctx => {
    const {id} = ctx.query
    const menu = await Menu.findById(id).populate('pid')
    ctx.body = {
        code: 200,
        msg: '查询成功',
        data: {
            menu
        }
    }
})

router.put('/menus/info', async ctx => {
    const {id} = ctx.query
    const {path, name, title} = ctx.request.body
    await Menu.findByIdAndUpdate(id, {path, name, title})
    ctx.body = {
        code: 200,
        msg: '修改成功',
        data: ''
    }
})

router.post('/menus/addition', async ctx => {
    const {name} = ctx.request.body
    const isExist = await Menu.findOne({name})
    if (isExist) {
        ctx.status = 400
        return ctx.body = {
            code: 400,
            msg: '该路由已存在'
        }
    }
    const menu = await Menu.create(ctx.request.body)
    ctx.body = {
        code: 200,
        msg: '创建成功',
        data: {
            menu
        }
    }
})

async function deleteAll(id) {
    const menus = await Menu.find({pid: id})
    if (menus.length === 0) {
        return
    }
    for (const menu of menus) {
        await Menu.findByIdAndDelete(menu._id)
        await deleteAll(menu._id)
    }
}

router.delete('/menus/delete', async ctx => {
    const {id} = ctx.query
    await Menu.findByIdAndDelete(id)
    await deleteAll(id)
    ctx.body = {
        code: 200,
        msg: '删除成功',
        data: ''
    }
})

module.exports = router
